// return the user data from the session storage
export const getUser = () => {
    const userStr = sessionStorage.getItem('user');
    if (userStr) return JSON.parse(userStr);
    else return null;
}

// return the token from the session storage
export const getToken = () => {
    const token = {
        'accessToken': sessionStorage.getItem('accessToken'),
        'idToken': sessionStorage.getItem('idToken'),
        'refreshToken': sessionStorage.getItem('refreshToken'),
    };

    return token || null;
}

// remove the token and user from the session storage
export const removeUserSession = () => {
    localStorage.removeItem('user');
    localStorage.removeItem('accessToken');
    localStorage.removeItem('idToken');
    localStorage.removeItem('refreshToken');
}

// set the token and user from the session storage
export const setUserSession = (accessToken, idToken, refreshToken) => {
    var user = { accessToken: accessToken, idToken: idToken, refreshToken: refreshToken };
    console.log(")))))))))))))))))))))" + idToken.payload.email);
    sessionStorage.setItem('accessToken', accessToken);
    sessionStorage.setItem('idToken', idToken);
    sessionStorage.setItem('refreshToken', refreshToken);
    //sessionStorage.setItem('token', token);
    sessionStorage.setItem('user', user); //JSON.stringify(idToken.payload.email));
}

export const loadState = () => {

    try {
        const serializedState = localStorage.getItem('user');
        if (serializedState === null)
            return undefined;
        return JSON.parse(serializedState);
    }
    catch (e) {
        return e;
    }
}

export const saveState = (state) => {
    try {
        const serializedState = JSON.stringyfy(state);
        localStorage.setItem('user', serializedState);
    }
    catch (e) {
        return e;
    }

}