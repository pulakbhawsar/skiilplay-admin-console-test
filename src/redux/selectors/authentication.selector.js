import { createSelector } from 'reselect'

const userSelector = (state) => state.authentication.user;//get('user');

export const getUser = createSelector(
    userSelector,
    (user) => ({
        email: user.email,
    })
);
