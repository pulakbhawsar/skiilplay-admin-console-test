import { call, put, takeEvery } from 'redux-saga/effects';
import { awsService } from '../../networks/aws';
import { alertConstants, authConstants } from '../constants';
import { history } from '../../helpers';

var message;
export function* loginSaga(user) {
    try {
        var user = yield call(awsService.signInUser, user.user);
        message = "Login Successful";
        //  user = {
        //     accessToken: {
        //         payload: {
        //             username: "bapatchinmayee@gmail.com"
        //         },
        //     },
        //     idToken: {
        //         payload: {
        //             email: "bapatchinmayee@gmail.com"
        //         }
        //     }
        // };

        // console.log(user, "kaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        // console.log(user + "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
        yield put({ type: alertConstants.SUCCESS, message });
        yield put({ type: authConstants.LOGIN_SUCCESS, user });
        history.push("/home");


    } catch (e) {
        message = e.message;
        yield put({ type: alertConstants.ERROR, message });
        yield put({ type: authConstants.LOGIN_FAILURE, e });
    }
}

export function* watchLogin() {
    yield takeEvery(authConstants.LOGIN_REQUEST, loginSaga);
}

export function* logoutSaga() {
    try {
        const responseBody = yield call(awsService.logout);
        //    console.log("LLLLLLLLLLOOOOOOOOOOOOOOGGGGGGGGGGGGOOOOOOOOOOOUUUUUUUUUUUUUTTTTTTTTTTTT");
        yield put({ type: alertConstants.SUCCESS, message });
        yield put({ type: authConstants.LOGOUT_SUCCESS, responseBody });
        history.push("/");//, responseBody);

    } catch (e) {
        message = e.message;
        yield put({ type: alertConstants.ERROR, message });
        yield put({ type: authConstants.LOGOUT_FAILURE, e });
    }

}

export function* watchLogout() {
    yield takeEvery(authConstants.LOGOUT_REQUEST, logoutSaga);
}
