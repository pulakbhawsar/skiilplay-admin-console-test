import { fork, all } from 'redux-saga/effects';
import { watchLogin, watchLogout } from './auth.saga';
export default function* rootSaga() {
    yield all([
        fork(watchLogin),
        fork(watchLogout),
    ]);
}
