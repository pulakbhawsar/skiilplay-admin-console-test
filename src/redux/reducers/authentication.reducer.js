import { authConstants } from '../constants';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
    switch (action.type) {
        case authConstants.LOGIN_REQUEST:
            return {
                loggingIn: true,
                user: action.user
            };
        case authConstants.LOGIN_SUCCESS:
            return {
                loggedIn: true,
                user: action.user
            };
        case authConstants.LOGIN_FAILURE:
            return {};
        case authConstants.LOGOUT_REQUEST:
            return {
                loggingIn: false,
                user: action.user
            };
        case authConstants.LOGOUT_SUCCESS:
            return {
                loggedIn: false,
                user: action.user
            };
        case authConstants.LOGOUT_FAILURE:
            return {};
        default:
            return state;
    }
}