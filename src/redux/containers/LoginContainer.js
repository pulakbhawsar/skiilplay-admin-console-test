import Login from '../../components/Login';
import { connect } from 'react-redux';
import { alertActions, authActions } from '../../redux/actions';

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    const { type, message } = state.alert;
    return { loggingIn, type, message };
}

const actionCreators = {
    login: authActions.login_request,
    logout: authActions.logout_request,
    error: alertActions.error,
    success: alertActions.success
};

export default connect(mapStateToProps, actionCreators)(Login);


