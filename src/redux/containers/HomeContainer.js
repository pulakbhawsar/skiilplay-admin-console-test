import Home from '../../components/Home';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { alertActions } from '../actions';
import { getUser } from '../selectors/authentication.selector';
import { withStyles } from '@material-ui/core/styles';
import { useStyles } from '../../components/Home/Home';

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    const { message, type } = state.alert;
    const user = getUser(state);
    return { user, loggingIn, message, type };
}

const actionCreators = {
    alert_error: alertActions.error,
    alert_success: alertActions.success
}

//export default connect(mapStateToProps, actionCreators)(Home);
export default compose(connect(mapStateToProps,
    actionCreators, // or put null here if you do not have actions to dispatch
), withStyles(useStyles),
)(Home);