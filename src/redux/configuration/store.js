import { createStore, applyMiddleware } from 'redux';
import 'babel-polyfill';
import "regenerator-runtime/runtime";
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import rootSaga from '../sagas';
import rootReducer from '../reducers';
import { loadState, saveState } from '../../helpers/common';
const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger();

export const store = createStore(
    rootReducer,
    applyMiddleware(
        sagaMiddleware,
        loggerMiddleware
    )
);
sagaMiddleware.run(rootSaga);

store.subscribe(() => saveState({ user: store.getState().user }))