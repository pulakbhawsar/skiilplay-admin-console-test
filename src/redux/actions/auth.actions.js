import { authConstants } from '../constants';

export const authActions = {
    login_failure,
    login_success,
    login_request,

    logout_request,
    logout_success,
    logout_failure,

};

function login_request(user) { return { type: authConstants.LOGIN_REQUEST, user } }
function login_success(user) { return { type: authConstants.LOGIN_SUCCESS, user } }
function login_failure(error) { return { type: authConstants.LOGIN_FAILURE, error } }

function logout_request() { return { type: authConstants.LOGOUT_REQUEST } }
function logout_success(user) { return { type: authConstants.LOGOUT_SUCCESS, user } }
function logout_failure(error) { return { type: authConstants.LOGOUT_FAILURE, error } }

