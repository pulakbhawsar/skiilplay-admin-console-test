import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { PrivateRoute } from './PrivateRoute';
import { history } from '../helpers';
import Login from '../redux/containers/LoginContainer';
import Home from '../redux/containers/HomeContainer';
import { GameCatlogue } from '../components/commonComponents';
function Routes() {
    return (
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={Login} />
                <Route path="/login" component={Login} />
                <PrivateRoute exact path="/home" component={Home} />
                <PrivateRoute exact path="/gameCatalogue" component={GameCatlogue} />
                <Redirect from="*" to="/" />
                <Redirect from="/" to="/login" />

            </Switch>
        </Router>

    );
}

export { Routes };