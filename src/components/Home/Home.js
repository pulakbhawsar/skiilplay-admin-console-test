import React from 'react';
import { createStyles } from '@material-ui/core/styles';
import {
    isMobile
} from "react-device-detect";
import { SideBar } from '../commonComponents/Sidebar';
import { Dashboard } from '../commonComponents/Dashboard';
const drawerWidth = 260;
//export const useStyles = makeStyles((theme) => ({
export const useStyles = (theme) => createStyles({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },

    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },

    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
});

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = { open: isMobile ? false : true };
    }
    render() {
        var classes = this.props;
        return (
            <div className={classes.root}>
                <SideBar classes={classes} isMobile={isMobile} user={this.props.user.email} />
                <Dashboard classes={classes} open={this.state.open} />
            </div>
        );

    }
}

export default Home;

{/*  <div className="col-lg-8 offset-lg-2" >
                <h1>Hi !  {this.props.user.email}</h1>
                <p> <Link to="/login">Logout</Link>
                </p>
            </div> */ }
