import React from 'react';
import { Link } from 'react-router-dom'

function Footer() {

    return (
        <footer>
            <div className="copy-right">© copyright 2021 <Link to="/home">SkillPlay</Link> All rights reserved.</div>
        </footer>
    )
}
export { Footer };