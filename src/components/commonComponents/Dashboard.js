import React from 'react';
import clsx from 'clsx';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Footer } from '../commonComponents';
import Grid from '@material-ui/core/Grid';

function Dashboard(props) {
    const { classes, open } = props;
    return (
        <div>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                }) + ' main-wrapper'}
            >
                <div className="main-content">
                    <div className="content">
                        <Box className="page-header">
                            <Typography variant="h2" className="page-name">Dashboard</Typography>
                        </Box>
                        <Grid container spacing={3}>
                            <Grid item xs={12} sm={12} md={6} lg={3}>
                                <Box p={3} className="dark-box dashboard-card">
                                    <div className="box-info">
                                        <h6>Total Players</h6>
                                        <Box display="flex" alignItems="center" justifyContent="space-between">
                                            <div className="box-count">6,382</div>
                                            <div className="box-icon blue">
                                                <i className="icon icon-graph"></i>
                                            </div>
                                        </Box>
                                        <span className="profit-loss"><i className="icon icon-up-arrow increase"></i><span>30% Increase</span></span>
                                    </div>
                                </Box>
                            </Grid>
                            <Grid item xs={12} sm={12} md={6} lg={3}>
                                <Box p={3} className="dark-box dashboard-card">
                                    <div className="box-info">
                                        <h6>Top Games</h6>
                                        <Box display="flex" alignItems="center" justifyContent="space-between">
                                            <div className="box-count">245</div>
                                            <div className="box-icon yellow">
                                                <i className="icon icon-graph"></i>
                                            </div>
                                        </Box>
                                        <span className="profit-loss"><i className="icon icon-up-arrow increase"></i><span>30% Increase</span></span>
                                    </div>
                                </Box>
                            </Grid>
                            <Grid item xs={12} sm={12} md={6} lg={3}>
                                <Box p={3} className="dark-box dashboard-card">
                                    <div className="box-info">
                                        <h6>Total Active Challenges</h6>
                                        <Box display="flex" alignItems="center" justifyContent="space-between">
                                            <div className="box-count">7,637</div>
                                            <div className="box-icon green">
                                                <i className="icon icon-graph"></i>
                                            </div>
                                        </Box>
                                        <span className="profit-loss"><i className="icon icon-up-arrow decrease"></i><span>8% Decrease</span></span>
                                    </div>
                                </Box>
                            </Grid>
                            <Grid item xs={12} sm={12} md={6} lg={3}>
                                <Box p={3} className="dark-box dashboard-card">
                                    <div className="box-info">
                                        <h6>Revenue</h6>
                                        <Box display="flex" alignItems="center" justifyContent="space-between">
                                            <div className="box-count">$7,850</div>
                                            <div className="box-icon orange">
                                                <i className="icon icon-graph"></i>
                                            </div>
                                        </Box>
                                        <span className="profit-loss"><i className="icon icon-up-arrow increase"></i><span>30% Increase</span></span>
                                    </div>
                                </Box>
                            </Grid>
                            <Grid item xs={12} sm={12} lg={6}>
                                <Box className="dark-box">
                                    <Box px={3} py={2} className="box-heading">Active Users</Box>
                                    <Box p={3} className="chart-unit">Chart Code Paste Here</Box>
                                </Box>
                            </Grid>
                            <Grid item xs={12} sm={12} lg={6}>
                                <Box className="dark-box">
                                    <Box px={3} py={2} display="flex" alignItems="center" justifyContent="space-between" className="box-heading">
                                        <span>Total Revenue</span>
                                        <div className="toggle-btn">
                                            <button className="btn active">Today</button>
                                            <button className="btn">This Month</button>
                                        </div>
                                    </Box>
                                    <Box p={3} className="chart-unit">Chart Code Paste Here</Box>
                                </Box>
                            </Grid>
                        </Grid>

                    </div>
                </div>
                <Footer />
            </main>
        </div>
    );
}

export { Dashboard };