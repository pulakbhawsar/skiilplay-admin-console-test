import React, { useEffect } from 'react';
import { Link } from 'react-router-dom'
import clsx from 'clsx';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import logo from "../../resources/images/skillplay-logo.png";
import Box from '@material-ui/core/Box';
import { Footer } from './Footer';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import userImg from "../../resources/images/icons/user-management.svg";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


import {
    BrowserView,
    MobileView,
    isBrowser,
    isMobile
} from "react-device-detect";
import { fontSize } from '@material-ui/system';

// For Table 
const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: "#191d31",
        color: "#ffffff",
        fontWeight: "600",
        paddingTop: "8px",
        paddingBottom: "8px",
        borderBottom: "2px solid rgb(108 114 146)"
    },
    body: {
        fontSize: 15,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({}))(TableRow);




// For sidebar and header
const drawerWidth = 260;
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },

    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },

    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));


function GameCatlogue() {

    // for drawer
    const classes = useStyles();
    const [open, setOpen] = React.useState(isMobile ? false : true);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };


    return (
        <div className={classes.root}>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar + ' header', {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar className="header-inner">
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <IconButton onClick={handleDrawerClose}
                        className={clsx(classes.menuButton, open || classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <ul className="header-right-section">
                        <li className="notifications"><i className="icon icon-notification-bell"></i></li>
                        <li>
                            <Box className="user-section">
                                <Box display="flex" alignItems="center">
                                    <Avatar alt="P" src={userImg} className={classes.large} />
                                    <Box ml={1} className="user-info">
                                        <Box component="span" className="welcome">Welcome</Box>
                                        <Typography variant="h5" className="user-name">John Doe</Typography>
                                    </Box>
                                </Box>
                            </Box>
                        </li>
                    </ul>

                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer + ' sidebar', {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
            >
                <div className="sidebar-logo"><img src={logo} alt="" className="img-fluid" /></div>
                <List className="navigation">
                    <ListItem>
                        <Link to="/home" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-dashboard"></i></ListItemIcon>
                            <ListItemText primary="Dashboard" />
                        </Link >
                    </ListItem>
                    <ListItem>
                        <Link to="/home" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-user-management"></i></ListItemIcon>
                            <ListItemText primary="User Management" />
                        </Link >
                    </ListItem>
                    <ListItem>
                        <Link to="/home" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-athlete"></i></ListItemIcon>
                            <ListItemText primary="Player Management" />
                        </Link >
                    </ListItem>
                    <ListItem>
                        <Link to="/home" className="nav-link active">
                            <ListItemIcon className="menu-icon"><i className="icon icon-puzzle"></i></ListItemIcon>
                            <ListItemText primary="Game Catalogue" />
                        </Link >
                    </ListItem>
                    <ListItem>
                        <Link to="/home" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-problem-solving"></i></ListItemIcon>
                            <ListItemText primary="Challenges" />
                        </Link >
                    </ListItem>
                    <ListItem>
                        <Link to="/home" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-settings"></i></ListItemIcon>
                            <ListItemText primary="Configuration" />
                        </Link >
                    </ListItem>
                </List>
                <List className="logout">
                    <ListItem>
                        <Link to="/" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-logout"></i></ListItemIcon>
                            <ListItemText primary="Logout" />
                        </Link >
                    </ListItem>
                </List>
            </Drawer>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                }) + ' main-wrapper'}
            >
                <div className="main-content">
                    <div className="content">
                        <Box className="page-header">
                            <Typography variant="h2" className="page-name">Game Catalogue</Typography>
                        </Box>
                        <Grid container className="table-grid-container">
                            <Grid item xs={12}>
                                <Box display="flex" justifyContent="flex-end" mb={3}>
                                    <button className="btn btn-primary"><Box mr={1}><i className="icon icon-plus"></i></Box> Add New Game</button>
                                </Box>
                                <Box className="dark-box">
                                    <Box px={3} py={2} className="box-heading">Games List</Box>
                                    <TableContainer component={Paper}>
                                        <Table
                                            className="table-grid"
                                            aria-label="sticky table"
                                            size=""
                                        >
                                            <TableHead>
                                                <TableRow>
                                                    <StyledTableCell width="15%">Name</StyledTableCell>
                                                    <StyledTableCell>Producer</StyledTableCell>
                                                    <StyledTableCell>Category</StyledTableCell>
                                                    <StyledTableCell>Created Date</StyledTableCell>
                                                    <StyledTableCell>Updated Date</StyledTableCell>
                                                    <StyledTableCell width="5%" align="right">Active Players</StyledTableCell>
                                                    <StyledTableCell align="right">Version</StyledTableCell>
                                                    <StyledTableCell align="center">Status</StyledTableCell>
                                                    <StyledTableCell width="5%" align="center">Action</StyledTableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                <StyledTableRow>
                                                    <StyledTableCell>Wrecking Ball</StyledTableCell>
                                                    <StyledTableCell>James</StyledTableCell>
                                                    <StyledTableCell>Action game</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell align="right">705</StyledTableCell>
                                                    <StyledTableCell align="right">1.0</StyledTableCell>
                                                    <StyledTableCell align="center"><span className="status active">Active</span></StyledTableCell>
                                                    <StyledTableCell align="center">
                                                        <button className="btn-icon"><i className="icon icon-edit"></i></button>
                                                        <button className="btn-icon"><i className="icon icon-trash"></i></button>
                                                    </StyledTableCell>
                                                </StyledTableRow>

                                                <StyledTableRow>
                                                    <StyledTableCell>Pokey Ball</StyledTableCell>
                                                    <StyledTableCell>James</StyledTableCell>
                                                    <StyledTableCell>Action game</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell align="right">705</StyledTableCell>
                                                    <StyledTableCell align="right">1.0</StyledTableCell>
                                                    <StyledTableCell align="center"><span className="status active">Active</span></StyledTableCell>
                                                    <StyledTableCell align="center">
                                                        <button className="btn-icon"><i className="icon icon-edit"></i></button>
                                                        <button className="btn-icon"><i className="icon icon-trash"></i></button>
                                                    </StyledTableCell>
                                                </StyledTableRow>
                                                <StyledTableRow>
                                                    <StyledTableCell>Parking Jam</StyledTableCell>
                                                    <StyledTableCell>James</StyledTableCell>
                                                    <StyledTableCell>Action game</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell align="right">705</StyledTableCell>
                                                    <StyledTableCell align="right">1.0</StyledTableCell>
                                                    <StyledTableCell align="center"><span className="status inactive">Inactive</span></StyledTableCell>
                                                    <StyledTableCell align="center">
                                                        <button className="btn-icon"><i className="icon icon-edit"></i></button>
                                                        <button className="btn-icon"><i className="icon icon-trash"></i></button>
                                                    </StyledTableCell>
                                                </StyledTableRow>
                                                <StyledTableRow>
                                                    <StyledTableCell>Word Guess</StyledTableCell>
                                                    <StyledTableCell>James</StyledTableCell>
                                                    <StyledTableCell>Action game</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell align="right">705</StyledTableCell>
                                                    <StyledTableCell align="right">1.0</StyledTableCell>
                                                    <StyledTableCell align="center"><span className="status pending">Pending</span></StyledTableCell>
                                                    <StyledTableCell align="center">
                                                        <button className="btn-icon"><i className="icon icon-edit"></i></button>
                                                        <button className="btn-icon"><i className="icon icon-trash"></i></button>
                                                    </StyledTableCell>
                                                </StyledTableRow>
                                                <StyledTableRow>
                                                    <StyledTableCell>Knock Balls</StyledTableCell>
                                                    <StyledTableCell>James</StyledTableCell>
                                                    <StyledTableCell>Action game</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell>20-12-2020</StyledTableCell>
                                                    <StyledTableCell align="right">705</StyledTableCell>
                                                    <StyledTableCell align="right">1.0</StyledTableCell>
                                                    <StyledTableCell align="center"><span className="status active">Active</span></StyledTableCell>
                                                    <StyledTableCell align="center">
                                                        <button className="btn-icon"><i className="icon icon-edit"></i></button>
                                                        <button className="btn-icon"><i className="icon icon-trash"></i></button>
                                                    </StyledTableCell>
                                                </StyledTableRow>

                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Box>
                            </Grid>
                        </Grid>
                    </div>
                </div>
                <Footer />
            </main>
        </div>
    );
}

export { GameCatlogue };