import React from 'react';
import { Link } from 'react-router-dom';
import clsx from 'clsx';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import logo from "../../resources/images/skillplay-logo.png";
import Box from '@material-ui/core/Box';
import { Footer } from '../commonComponents';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import userImg from "../../resources/images/icons/user-management.svg";
// For sidebar and header
const drawerWidth = 260;

function SideBar(props) {

    const { classes } = props.classes;//useStyles();
    const [open, setOpen] = React.useState(props.isMobile ? false : true);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar + ' header', {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar className="header-inner">
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <IconButton onClick={handleDrawerClose}
                        className={clsx(classes.menuButton, open || classes.hide)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <ul className="header-right-section">
                        <li className="notifications"><i className="icon icon-notification-bell"></i></li>
                        <li>
                            <Box className="user-section">
                                <Box display="flex" alignItems="center">
                                    <Avatar alt="P" src={userImg} className={classes.large} />
                                    <Box ml={1} className="user-info">
                                        <Box component="span" className="welcome">Welcome</Box>
                                        <Typography variant="h5" className="user-name">{props.user}</Typography>
                                    </Box>
                                </Box>
                            </Box>
                        </li>
                    </ul>

                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer + ' sidebar', {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
            >
                <div className="sidebar-logo"><img src={logo} alt="" className="img-fluid" /></div>
                <List className="navigation">
                    <ListItem>
                        <Link to="/home" className="nav-link active">
                            <ListItemIcon className="menu-icon"><i className="icon icon-dashboard"></i></ListItemIcon>
                            <ListItemText primary="Dashboard" />
                        </Link>
                    </ListItem>
                    <ListItem>
                        <Link to="/home" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-user-management"></i></ListItemIcon>
                            <ListItemText primary="User Management" />
                        </Link>
                    </ListItem>
                    <ListItem>
                        <Link to="/home" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-athlete"></i></ListItemIcon>
                            <ListItemText primary="Player Management" />
                        </Link>
                    </ListItem>
                    <ListItem>
                        <Link to="/gameCatalogue" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-puzzle"></i></ListItemIcon>
                            <ListItemText primary="Game Catalogue" />
                        </Link>
                    </ListItem>
                    <ListItem>
                        <Link to="/home" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-problem-solving"></i></ListItemIcon>
                            <ListItemText primary="Challenges" />
                        </Link>
                    </ListItem>
                    <ListItem>
                        <Link to="/home" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-settings"></i></ListItemIcon>
                            <ListItemText primary="Configuration" />
                        </Link>
                    </ListItem>
                </List>
                <List className="logout">
                    <ListItem>
                        <Link to="/" className="nav-link">
                            <ListItemIcon className="menu-icon"><i className="icon icon-logout"></i></ListItemIcon>
                            <ListItemText primary="Logout" />
                        </Link>
                    </ListItem>
                </List>
            </Drawer>
        </React.Fragment>
    );

}

export { SideBar };