import React from 'react';
import { Link } from 'react-router-dom';
import logo from "../../resources/images/skillplay-logo.png";
import TextField from '@material-ui/core/TextField';

class Login extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.logout();

        this.state = {
            email: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { email, password } = this.state;
        console.log(email + '2222222222' + password);
        if (email && password) {
            var user = { email: email, password: password };
            this.props.login(user);
        }
        else
            this.props.error('Please enter valid creadentials');
    }

    render() {
        const { loggingIn, type, message } = this.props;
        const { email, password, submitted } = this.state;
        return (
            <div className="login-wrapper" >
                <div className="login-box-outer">
                    <div className="login-bg">
                        <div>
                            <a href="index.html" className="login-logo"><img src={logo} alt="" className="img-fluid" /></a>
                            <h2>Welcome Back!</h2>
                            <p>To keep connected with us please with your personal info</p>

                        </div>

                    </div>
                    <div className="login-box">
                        <form onSubmit={this.handleSubmit}>
                            <div className="box-heading">
                                <h5 className="text-center">Sign In</h5>
                                <p>Please enter your credentials</p>
                                <div className="d-flex alertMessage"
                                    style={{
                                        minWidth: 400,
                                        justifyContent: 'space-between'
                                    }}>
                                    <span>{message}</span>
                                </div>
                            </div>

                            <div className="login-fields">
                                <div className="form-field">
                                    <TextField
                                        label="Email"
                                        type="username"
                                        fullWidth={true}
                                        name="email"
                                        value={email}
                                        onChange={this.handleChange}
                                    />
                                    {submitted && !email &&
                                        <div className="help-block">Username is required</div>
                                    }
                                </div>
                                <div className="form-field">
                                    <TextField
                                        type="password"
                                        label="Password"
                                        fullWidth={true}
                                        name="password"
                                        value={password}
                                        onChange={this.handleChange}
                                    />
                                    {submitted && !password &&
                                        <div className="help-block">Password is required</div>
                                    }
                                </div>
                                <div className="form-field text-right">
                                    <Link to="/ForgotPassword" className="link-forgot-password">Forgot Password?</Link>
                                </div>
                            </div>

                            <div className="btn-group text-center">
                                <button type="submit" className="btn btn-primary">
                                    {loggingIn &&
                                        <img
                                            src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="
                                            alt="Loading" />
                                    }&nbsp;Sign In
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div >
        );
    }
}
export default Login;



