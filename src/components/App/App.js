import React, { useEffect, useState } from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { history } from '../../helpers';
import { alertActions } from '../../redux/actions';
import { Routes } from '../../routes/Routes';
import { Home } from '../Home/Home';
import { Login } from '../Login/Login';
import '../../resources/css/App.scss';

function App() {
    // const alert = useSelector(state => state.alert);
    // const dispatch = useDispatch();

    // const [modalDisplay, toggleDisplay] = useState('none');
    // const openModal = () => { toggleDisplay('block'); }
    // const closeModal = () => { toggleDisplay('none'); }

    // useEffect(() => {
    //     history.listen((location, action) => {
    //         // clear alert on location change
    //         dispatch(alertActions.clear());
    //     });
    //     if (alert.message !== null) {
    //         openModal();
    //     } else {
    //         closeModal();
    //     }
    // }, []);

    return (
        <div className="App h-100" >
            <Routes />
        </div>);
}



export default App;

{/* // 
//     <div className={`alert ${alert.type}`}>{alert.message}</div>
// } */}
