
import { CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';

import { userPool } from '../constants';
import { setUserSession, removeUserSession, getUser } from '../../helpers';


export const awsService = {
    login,
    logout,
    getUserDetails, signInUser
};

async function login(user) {
    const p = await new Promise((resolve, reject) => {
        // console.log(user.email + user.password + "^^^^^^^^^^^^^^^^^^^^^^^");
        const profile = new CognitoUser({
            Username: user.email,
            Pool: userPool
        });
        const authDetails = new AuthenticationDetails({
            Username: user.email,
            Password: user.password
        });
        profile.authenticateUser(authDetails, {
            onSuccess: data => {
                console.log('onSuccess', data);
                setUserSession(data.accessToken, data.idToken, data.refreshToken);
                resolve(data);
            },
            onFailure: err => {
                console.log('onFailure', err);
                reject(err);
            },
            newPasswordRequired: data => {
                console.log('newPasswordRequired', data);
                resolve(data);
            }
        });
    });
    return p;
}


function logout() {
    // remove user from local storage to log user out
    removeUserSession();
}

async function getUserDetails(email) {
    const p = await new Promise((resolve, reject) => {
        const userData = {
            Username: email,
            Pool: userPool
        };
        const cognitoUser = new CognitoUser(userData);

        buildUserObject(cognitoUser)
            .then((userProfileObject) => {
                resolve(userProfileObject);
            })
            .catch((err) => {
                reject(err);
            });

    });
    return p;
}





async function signInUser(user) {
    const p = await new Promise((resolve, reject) => {
        const authenticationDetails = new AuthenticationDetails({
            Username: user.email,
            Password: user.password
        });
        const userData = {
            Username: user.email,
            Pool: userPool
        };
        const cognitoUser = new CognitoUser(userData);
        authenticateUser(cognitoUser, authenticationDetails)
            .then(() => {
                return buildUserObject(cognitoUser);
            })
            .then((userProfileObject) => {
                var user = JSON.stringify(userProfileObject);
                localStorage.setItem('user', user);
                resolve(userProfileObject);
            })
            .catch((err) => {
                reject(err);
            });
    })
    return p;
}
function authenticateUser(cognitoUser, authenticationDetails) {
    const p = new Promise((resolve, reject) => {
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                console.log('onSuccess', result);
                setUserSession(result.accessToken, result.idToken, result.refreshToken);
                // const loginsObj = {
                //     // For the object's key name, use the USERPOOL_ID taken from our shared aws_profile js file
                //     // For the object's value, use the jwtToken received in the success callback
                //     [USERPOOL_ID]: result.getIdToken().getJwtToken()
                // }
                // AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                //     IdentityPoolId: IDENTITY_POOL_ID, // your identity pool id here
                //     Logins: loginsObj
                // })
                // AWS.config.credentials.refresh(function () {
                //     console.log(AWS.config.credentials)
                // })
                resolve();
            },
            onFailure: function (err) {
                console.log(err);
                reject(err);
            },
        });
    });
    return p;
}

function buildUserObject(cognitoUser) {
    const p = new Promise((resolve, reject) => {
        cognitoUser.getUserAttributes(function (err, result) {
            if (err) {
                console.log(err);
                reject(err);
            }
            let userProfileObject = {};
            for (let i = 0; i < result.length; i++) {
                if (result[i].getName().indexOf('custom:') >= 0) {
                    let name = result[i].getName().slice(7, result[i].getName().length)
                    userProfileObject[name] = result[i].getValue()
                } else {
                    userProfileObject[result[i].getName()] = result[i].getValue()
                }
            }
            // console.log(userProfileObject + 'userProfileObject');
            resolve(userProfileObject);
        });
    });
    return p;
}

