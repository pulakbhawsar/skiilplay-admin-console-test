import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { store } from '../src/redux/configuration';
import App from '../src/components/App/App';

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);